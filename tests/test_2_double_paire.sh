#!/bin/bash

#fichier de lancement des tests, permet de vérifier que tout est OK et qu'il n'y pas de régression

#test 2 - analyse de la main - Paire
echo "--- DEBUT DU TEST 2 - ANALYSE DE LA MAIN - DOUBLE_PAIRE ---"

#definition des variables
fichier_cartes_joueur1="tmp/cartes_joueurs1_double_paire"
fichier_cartes_neutres="tmp/cartes_neutres_double_paire"

#appel des fonctions
echo "Appel du fichier de fonctions $fichier_fonctions"
fichier_fonctions="../scripts/fonctions.sh"
source $fichier_fonctions

echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

resultat="$( analyseMain |tail -1)"
resultat_attendu="double_paire;Q;4;2"
#echo "$resultat ||| $resultat_attendu"

if [[ "$resultat" == "$resultat_attendu" ]]
then
	echo 'TEST 2 OK'
else
	echo '/!\ TEST 2 EN ECHEC /!\ --'"$resultat ||| $resultat_attendu"
fi
echo "--- FIN DU TEST 2 - ANALYSE DE LA MAIN - DOUBLE_PAIRE ---"

#!/bin/bash

#fichier de lancement des tests, permet de vérifier que tout est OK et qu'il n'y pas de régression

#test 4 - analyse de la main - Carré
echo "--- DEBUT DU TEST 4 - ANALYSE DE LA MAIN - CARRE ---"

#definition des variables
fichier_cartes_joueur1="tmp/cartes_joueurs1_carre"
fichier_cartes_neutres="tmp/cartes_neutres_carre"

#appel des fonctions
echo "Appel du fichier de fonctions $fichier_fonctions"
fichier_fonctions="../scripts/fonctions.sh"
source $fichier_fonctions

echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

resultat="$( analyseMain |tail -1)"
resultat_attendu="carre;5;2"
#echo "$resultat ||| $resultat_attendu"

if [[ "$resultat" == "$resultat_attendu" ]]
then
	echo 'TEST 4 OK'
else
	echo '/!\ TEST 4 EN ECHEC /!\ --'"$resultat ||| $resultat_attendu"
fi
echo "--- FIN DU TEST 4 - ANALYSE DE LA MAIN - CARRE ---"

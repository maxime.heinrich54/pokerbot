#!/bin/bash

#test 8 - analyse de la main - Suite
echo "--- DEBUT DU TEST 8 - ANALYSE DE LA MAIN - SUITE ---"

#definition des variables
fichier_cartes_joueur1="tmp/cartes_joueurs1_suite"
fichier_cartes_neutres="tmp/cartes_neutres_suite"

#chargement des fichiers de cartes
echo "8" > $fichier_cartes_joueur1
echo "J" >> $fichier_cartes_joueur1
echo "K" > $fichier_cartes_neutres
echo "9" >> $fichier_cartes_neutres
echo "10" >> $fichier_cartes_neutres
echo "7" >> $fichier_cartes_neutres

#appel des fonctions
echo "Appel du fichier de fonctions $fichier_fonctions"
fichier_fonctions="../scripts/fonctions.sh"
source $fichier_fonctions

echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

resultat="$( analyseMain |tail -1)"
#resultat="$( analyseMain )";echo "$resultat"
resultat_attendu="suite;J;2"
#echo "$resultat ||| $resultat_attendu"

if [[ "$resultat" == "$resultat_attendu" ]]
then
	echo 'TEST 8 OK'
else
	echo '/!\ TEST 8 EN ECHEC /!\ --'"$resultat ||| $resultat_attendu"
fi
echo "--- FIN DU TEST 8 - ANALYSE DE LA MAIN - SUITE ---"

#!/bin/bash

#test 6 - analyse de la main - Couleur
echo "--- DEBUT DU TEST 6 - ANALYSE DE LA MAIN - COULEUR - 1er test ---"

#definition des variables
fichier_cartes_joueur1="tmp/cartes_joueurs1_couleur_1"
fichier_cartes_neutres="tmp/cartes_neutres_couleur_1"

#appel des fonctions
echo "Appel du fichier de fonctions $fichier_fonctions"
fichier_fonctions="../scripts/fonctions.sh"
source $fichier_fonctions

echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

resultat="$( analyseMain |tail -1)"
#resultat="$( analyseMain )";echo "$resultat"
resultat_attendu="couleur;J;2"
#echo "$resultat ||| $resultat_attendu"

if [[ "$resultat" == "$resultat_attendu" ]]
then
	echo 'TEST 6 OK'
else
	echo '/!\ TEST 6 EN ECHEC /!\ --'"$resultat ||| $resultat_attendu"
fi
echo "--- FIN DU TEST 6 - ANALYSE DE LA MAIN - COULEUR - 1er test ---"

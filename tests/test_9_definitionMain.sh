#!/bin/bash

#test 9 - test de la fonction definitionCarte
echo "--- DEBUT DU TEST 9 - FONCTION definitionCarte ---"

#appel des fonctions
echo "Appel du fichier de fonctions $fichier_fonctions"
fichier_fonctions="../scripts/fonctions.sh"
source $fichier_fonctions

echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

echo $(definitionCarte 1 |cut -d';' -f1)
echo $(definitionCarte 2 |cut -d';' -f1)
echo $(definitionCarte 3 |cut -d';' -f1)
echo $(definitionCarte 4 |cut -d';' -f1)
echo $(definitionCarte 5 |cut -d';' -f1)
echo $(definitionCarte 6 |cut -d';' -f1)
echo $(definitionCarte 7 |cut -d';' -f1)
echo $(definitionCarte 8 |cut -d';' -f1)
echo $(definitionCarte 9 |cut -d';' -f1)
echo $(definitionCarte 10 |cut -d';' -f1)
echo $(definitionCarte 11 |cut -d';' -f1)
echo $(definitionCarte 12 |cut -d';' -f1)
echo $(definitionCarte 13 |cut -d';' -f1)

#if [[ "$resultat" == "$resultat_attendu" ]]
#then
#	echo 'TEST 8 OK'
#else
#	echo '/!\ TEST 8 EN ECHEC /!\ --'"$resultat ||| $resultat_attendu"
#fi
#echo "--- FIN DU TEST 8 - ANALYSE DE LA MAIN - SUITE ---"

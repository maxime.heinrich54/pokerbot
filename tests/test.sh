#!/bin/bash

#test 1 - analyse de la main - paire
bash test_1_paire.sh

#test 2 -  analyse de la main - double_paire
bash test_2_double_paire.sh

#test 3 - analyse de la main - brelan
bash test_3_brelan.sh

#test 4 - analyse de la main - carre
bash test_4_carre.sh

#test5 - analyse de la main - full
bash test_5_full.sh

#test6 - analyse de la main - couleur 1er test
bash test_6_couleur_1.sh

#test7 - analyse de la main - couleur 2eme test
bash test_7_couleur_2.sh

#test8 - analyse de la main - suite
bash test_8_suite.sh

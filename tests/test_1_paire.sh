#!/bin/bash

#fichier de lancement des tests, permet de vérifier que tout est OK et qu'il n'y pas de régression

#test 1 - analyse de la main - Paire
echo "--- DEBUT DU TEST 1 - ANALYSE DE LA MAIN - PAIRE ---"

#definition des variables
fichier_cartes_joueur1="tmp/cartes_joueurs1_paire"
fichier_cartes_neutres="tmp/cartes_neutres_paire"

#appel des fonctions
echo "Appel du fichier de fonctions $fichier_fonctions"
fichier_fonctions="../scripts/fonctions.sh"
source $fichier_fonctions

echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

resultat="$( analyseMain |tail -1)"
resultat_attendu="paire;K;1"
#echo "$resultat ||| $resultat_attendu"

if [[ "$resultat" == "$resultat_attendu" ]]
then
	echo 'TEST 1 OK'
else
	echo '/!\ TEST 1 EN ECHEC /!\ --'"$resultat ||| $resultat_attendu"
fi
echo "--- FIN DU TEST 1 - ANALYSE DE LA MAIN - PAIRE ---"

#!/bin/bash
#liste les tirages possibles pour 2 joueurs
fichier_liste_tirages="../tmp/liste_tirages_table"

echo "reinitialisation du fichier de tirage"
> $fichier_liste_tirages

echo "remplissage du fichier de tirage"
#pour éviter la production de doublon, on affiche les cartes dans l'ordre croissant
for carte1 in $(seq 1 48)
do
	echo "Avancement $carte1/48"
	for carte2 in $(seq $((carte1+1)) 49)
	do
	if [[ $carte2 -ne $carte1 ]]
	then
		for carte3 in $(seq $((carte2+1)) 50)
		do
		if [[ $carte3 -ne $carte1 ]]&&[[ $carte3 -ne $carte2 ]]
		then
			for carte4 in $(seq $((carte3+1)) 51)
			do
			if [[ $carte4 -ne $carte1 ]]&&[[ $carte4 -ne $carte2 ]]&&[[ $carte4 -ne $carte3 ]]
			then
				for carte5 in $(seq $((carte4+1)) 52)
				do
				if [[ $carte5 -ne $carte1 ]]&&[[ $carte5 -ne $carte2 ]]&&[[ $carte5 -ne $carte3 ]]&&[[ $carte5 -ne $carte4 ]]
				then
					#echo "ecriture de la ligne $carte1;$carte2;$carte3;$carte4;$carte5"
					echo "$carte1;$carte2;$carte3;$carte4;$carte5" >> $fichier_liste_tirages
				fi
				done
			fi
			done
		fi
		done
	fi
	done
done


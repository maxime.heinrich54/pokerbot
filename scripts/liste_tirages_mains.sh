#!/bin/bash
#liste les tirages possibles pour 2 joueurs
fichier_liste_tirages_main1="../tmp/liste_tirages_main1"
fichier_liste_tirages_main2="../tmp/liste_tirages_main2"

#appel des fonctions
fichier_fonctions="./fonctions.sh"
echo "Chargement des fonctions (fichier $fichier_fonctions)"
source $fichier_fonctions
echo "Fonctions importées : "
echo "$(export -f |grep declare |cut -d' ' -f3-)"

echo "reinitialisation des fichiers de tirage"
> $fichier_liste_tirages_main1
> $fichier_liste_tirages_main2

echo "debut du remplissage des fichiers de tirage"

#pour éviter la production de doublon, on affiche les cartes dans l'ordre croissant
for carte1 in $(seq $((carte3+1)) 51)
do
	echo "Avancement : $carte1/51"
	for carte2 in $(seq $((carte4+1)) 52)
	do
		if [[ $carte2 -ne $carte1 ]]
		then
			echo ";$carte1;$carte2;" >> $fichier_liste_tirages_main1
			echo ";$carte1;$carte2;" >> $fichier_liste_tirages_main2
		fi
	done
done

echo "Conversion des fichiers $fichier_liste_tirages_main1 et $fichier_liste_tirages_main2"
#conversion des ; en | pour adapter le format du fichier
sed -i 's/\;/\|/g' $fichier_liste_tirages_main1
sed -i 's/\;/\|/g' $fichier_liste_tirages_main2

#conversion des numeros de carte en couple hauteur;couleur
for carte in $(seq 1 52)
do
	defCarte=$(definitionCarte $carte)
	echo "$defCarte"
	sed -i "s/|$carte|/|$defCarte|/g" $fichier_liste_tirages_main1
	sed -i "s/|$carte|/|$defCarte|/g" $fichier_liste_tirages_main2
done

#!/bin/bash

#fichier contenant les fonctions utiles

distribution (){
	premier_tirage="true"
	num_carte=$((1 + $RANDOM % 52))
	while [[ "$( grep $num_carte $fichier_tirage)" != "" ]]||[[ $premier_tirage == "true" ]]
	do
		premier_tirage="false"
		#tirage d'un nombre entier aléatoire compris entre 1 et 52
		num_carte=$((1 + $RANDOM % 52))
	done
	echo "$num_carte" >> $fichier_tirage
	#echo "tirage de la carte $num_carte"
	resultatDefinition="$(definitionCarte $num_carte)"
	echo "$resultatDefinition"
}

donnerVainqueur2joueurs() {
#en parametre les mains des 2 joueurs et les cartes de la table
cartes_joueur1=$1
carte1_joueur1=$(echo $cartes_joueur1 |cut -d'|' -f1)
carte2_joueur1=$(echo $cartes_joueur1 |cut -d'|' -f2)
cartes_joueur2=$2
carte1_joueur2=$(echo $cartes_joueur2 |cut -d'|' -f1)
carte2_joueur2=$(echo $cartes_joueur2 |cut -d'|' -f2)
cartes_table=$3
carte1_table=$(echo $cartes_table |cut -d'|' -f1)
carte2_table=$(echo $cartes_table |cut -d'|' -f2)
carte3_table=$(echo $cartes_table |cut -d'|' -f3)
carte4_table=$(echo $cartes_table |cut -d'|' -f4)
carte5_table=$(echo $cartes_table |cut -d'|' -f5)

#on ecrit les cartes dans les fichiers
#ecriture dans le fichier des cartes de la table
#ecriture dans le fichier des cartes du joueur
meilleure_combinaison_joueur1="$(analyseMain)"

#ecriture dans le fichier des cartes du joueur
meilleure_combinaison_joueur2="$(analyseMain)"


#comparaison




}

definitionCarte (){
	#definition de la hauteur
	hauteur=$(expr $1 % 13)
	if [[ $hauteur -eq 0 ]]
        then
                hauteur="K"
        fi
	if [[ $hauteur -eq 11 ]]
	then
		hauteur="J"
	fi
	if [[ $hauteur -eq 12 ]]
        then
                hauteur="Q"
        fi
	if [[ $hauteur -eq 1 ]]
        then
                hauteur="A"
        fi
	#definition de la couleur
	if [[ $1 -le 13 ]]
	then
		couleur="COEUR"
	fi
	if [[ $1 -gt 13 ]]&&[[ $1 -le 26 ]]
        then
                couleur="CARREAU"
        fi
	if [[ $1 -gt 26 ]]&&[[ $1 -le 39 ]]
        then
                couleur="TREFLE"
        fi
        if [[ $1 -gt 39 ]]
        then
                couleur="PIQUE"
        fi
	echo "$hauteur;$couleur"

}

analyseMain (){
#chaque combinaison est recherchee, si elle est trouvée on doit determiner sa hauteur
#il faut egalement compter le nombre de carte de la main qui servent à cette combinaison
carte_la_plus_haute="false"
paire="false"
double_paire="false"
brelan="false"
carre="false"
couleur="false"
suite="false"
quinteFlush="false"
full="false"

meilleure_combinaison_paire="false"
meilleure_combinaison_double_paire="false"
meilleure_combinaison_brelan="false"
meilleure_combinaison_carre="false"
meilleure_combinaison_couleur="false"
meilleure_combinaison_suite="false"
meilleure_combinaison_quinteFlush="false"
meilleure_combinaison_full="false"

meilleure_combinaison="aucune"

liste_hauteurs=""
#recherche d'une paire, un brelan ou un carre et determiner la hauteur
for hauteur in 2 3 4 5 6 7 8 9 10 J Q K A
do
	nombre_occurence="$(cat $fichier_cartes_joueur1 $fichier_cartes_neutres |cut -d';' -f1 |grep -c $hauteur)"
	#carre
	if [[ $nombre_occurence -eq 2 ]]
	then
		echo "paire de $hauteur"
		if [[ "$paire" == "false" ]]
		then
			paire="true"
			hauteur_paire="$hauteur"
			echo "paire de $hauteur"
			#on regarde si il y a des cartes dans la main qui correspondent à cette paire
			main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c $hauteur)"
			meilleure_combinaison_paire="paire;$hauteur_paire;$main"
		else
			echo "double paire ($hauteur_paire et ancienne_hauteur_paire)"
			double_paire="true"
			ancienne_hauteur_paire="$hauteur_paire"
			hauteur_paire="$hauteur"
			hauteur_double_paire="$hauteur_paire;$ancienne_hauteur_paire"
			#on regarde si il y a des cartes dans la main qui correspondent à cette double paire
			main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e $hauteur_paire -e $ancienne_hauteur_paire)"
			meilleure_combinaison_double_paire="double_paire;$hauteur_double_paire;$main"
		fi

	fi
	#brelan
	if [[ $nombre_occurence -eq 3 ]]
        then
		brelan="true"
		hauteur_brelan="$hauteur"
                echo "brelan de $hauteur"
		#on regarde si il y a des cartes dans la main qui correspondent à ce brelan
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c $hauteur)"
		meilleure_combinaison_brelan="brelan;$hauteur_brelan;$main"
        fi
	#paire
	if [[ $nombre_occurence -eq 4 ]]
        then
		carre="true"
		hauteur_carre="$hauteur"
                echo "carre de $hauteur"
		#on regarde si il y a des cartes dans la main qui correspondent au carré
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c $hauteur)"
		meilleure_combinaison_carre="carre;$hauteur_carre;$main"
        fi

	#on note la carte la plus haute de la main, sert si il n'y a pas de combinaison
	if [[ "$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep $hauteur)" == "$hauteur" ]]
	then
		carte_la_plus_haute_de_la_main="$hauteur"
	fi
done

#recherche d'un full
if [[ $paire == "true" ]]&&[[ $brelan == "true" ]]
then
	full="true"
	hauteur_full="$hauteur_brelan;$hauteur_paire"
	echo "full de hauteur $hauteur_brelan et $hauteur_paire"
	#on regarde dans la main si des cartes correspondent au full
	main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e $hauteur_paire -e $hauteur_brelan)"
	meilleure_combinaison_full="full;$hauteur_full;$main"

fi

#recherche d'une couleur, pour les mains en cours on retourne le nombre de cartes de la couleur la plus présente
for color in TREFLE PIQUE CARREAU COEUR
do
	nb_couleur="$(cat $fichier_cartes_joueur1 $fichier_cartes_neutres |grep -c $color)"
	if [[ $nb_couleur -eq 5 ]]
	then
		couleur="true"

		hauteur_couleur=""
		#recherche de la hauteur, attention ne fonctionne pas avec un simple sort à cause des lettres
		for hauteur in 2 3 4 5 6 7 8 9 10 J Q K A
		do

			#on cherche la carte de la couleur la plus haute dans la main (si il y en a une)
			if [[ "$(cat $fichier_cartes_joueur1 |grep $color |cut -d';' -f1 |grep ^$hauteur$)" != "" ]]
			then
				hauteur_couleur_main="$hauteur"
			fi

			#si il n'y en a pas, cela veut dire que les 5 cartes "neutres" sont de la meme couleur, donc on cherche la carte la plus basse de la table
			if [[ $hauteur_couleur == "" ]]
			then
				hauteur_couleur="$(cat $fichier_cartes_neutres |grep $color |cut -d';' -f1 |grep ^$hauteur$)"

			fi
		done

		if [[ "$hauteur_couleur_main" != "" ]]
		then
			echo "couleur de hauteur $hauteur_couleur"
			#on regarde combien de cartes de la meme couleur sont SUR LA TABLE
			table="$(cat $fichier_cartes_neutres |cut -d';' -f2 |grep -c $color )"
			#on considere ensuite que si 4 cartes de la meme couleur sont sur la table, alors une seule carte de la main est utile à la couleur
			main=$((5-table))
			meilleure_combinaison_couleur="couleur;$hauteur_couleur_main;$main"
		else
			echo "couleur, mais pas de carte en main, la carte la plus basse de la table est $hauteur_couleur"
			#pas de carte en main, donc 5 cartes de la même couleur sur la table
			meilleure_combinaison_couleur="couleur;$hauteur_couleur;0"
		fi

	fi


done

#recherche d'une suite
#l'as peut servir aux deux extremités, donc on le fait apparaitre deux fois
liste_hauteurs_suite=""
for hauteur in A 2 3 4 5 6 7 8 9 10 J Q K A
do
	if [[ "$(cat $fichier_cartes_joueur1 $fichier_cartes_neutres |cut -d';' -f1 |grep $hauteur)" == "$hauteur" ]]
	then
		liste_hauteurs_suite="${liste_hauteurs_suite}O"
	else
		liste_hauteurs_suite="${liste_hauteurs_suite}N"
	fi
done

if [[ "$(echo $liste_hauteurs_suite |grep OOOOO)" == "" ]]
then
	#pas de suite
	meilleure_combinaison_suite="false"
else
	#suite trouvée
	schema_suite="$(echo $liste_hauteurs_suite |grep OOOOO)"

	if [[ $schema_suite =~ .........OOOOO ]]
	then
		hauteur_suite="A"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e A -e K -e Q -e J -e 10)"
	fi
	if [[ $schema_suite =~ ........OOOOO. ]]
	then
		hauteur_suite="K"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e K -e Q -e J -e 10 -e 9)"
	fi
	if [[ $schema_suite =~ .......OOOOO.. ]]
	then
		hauteur_suite="Q"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e Q -e J -e 10 -e 9 -e 8)"
	fi
	if [[ $schema_suite =~ ......OOOOO... ]]
	then
		hauteur_suite="J"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e J -e 10 -e 9 -e 8 -e 7)"
	fi
	if [[ $schema_suite =~ .....OOOOO.... ]]
	then
		hauteur_suite="10"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e 10 -e 9 -e 8 -e 7 -e 6)"
	fi
	if [[ $schema_suite =~ ....OOOOO..... ]]
	then
		hauteur_suite="9"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e 9 -e 8 -e 7 -e 6 -e 5)"
	fi
	if [[ $schema_suite =~ ...OOOOO...... ]]
	then
		hauteur_suite="8"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e 8 -e 7 -e 6 -e 5 -e 4)"
	fi
	if [[ $schema_suite =~ ..OOOOO....... ]]
	then
		hauteur_suite="7"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e 7 -e 6 -e 5 -e 4 -e 3)"
	fi

	if [[ $schema_suite =~ .OOOOO........ ]]
	then
		hauteur_suite="6"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e 6 -e 5 -e 4 -e 3 -e 2)"
	fi
	if [[ $schema_suite =~ OOOOO......... ]]
	then
		hauteur_suite="5"
		main="$(cat $fichier_cartes_joueur1 |cut -d';' -f1 |grep -c -e 5 -e 4 -e 3 -e 2 -e A)"
	fi

	meilleure_combinaison_suite="suite;$hauteur_suite;$main"
fi


#ici, toutes les combinaisons sont notées,  reste à trouver la meilleure
#pour les 2 premieres carte, on simplifie, soit paire, soit carte la plus haute de la main
#if [[ "$paire" == "false" ]];then echo "hauteur : $carte_la_plus_haute_de_la_main";else echo "paire : $paire";fi

#retour de la fonction, format "type de combinaison;hauteur,nombre de carte de la main"
#echo $meilleure_combinaison

if [[ "$meilleure_combinaison_quinteFlush" != "false" ]]
then
	meilleure_combinaison="$meilleure_combinaison_quinteFlush"
fi
if [[ "$meilleure_combinaison_carre" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_carre"
fi
if [[ "$meilleure_combinaison_full" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_full"
fi
if [[ "$meilleure_combinaison_couleur" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_couleur"
fi
if [[ "$meilleure_combinaison_suite" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_suite"
fi
if [[ "$meilleure_combinaison_brelan" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_brelan"
fi
if [[ "$meilleure_combinaison_double_paire" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_double_paire"
fi
if [[ "$meilleure_combinaison_paire" != "false" ]]&&[[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$meilleure_combinaison_paire"
fi

#si rien, alors juste la carte la plus haute
if [[ "$meilleure_combinaison" == "aucune" ]]
then
	meilleure_combinaison="$carte_la_plus_haute"
fi

#on retourne la meilleure combinaison (comprend la hauteur et le nombre de carte de la main impactées)
echo "$meilleure_combinaison"

}

export -f distribution
export -f donnerVainqueur2joueurs
export -f definitionCarte
export -f analyseMain







#!/bin/bash

#situation initiale, 3 joueurs avec 2 cartes chacun, seules les cartes du joueur 1 sont connues
#chaque carte est numérotée de 1 à 52 (de AS à ROI, puis par couleur : coeur, carreau, trèfle et pique)

#definition du fichier contenant les cartes tirées
fichier_tirage="../tmp/fichier_tirage"
fichier_cartes_joueur1="../tmp/cartes_joueurs1"
fichier_cartes_neutres="../tmp/cartes_neutres"

#appel des fonctions
fichier_fonctions="./fonctions.sh"
bash $fichier_fonctions

#lancement de la partie

nombre_de_joueurs=3

#on commence par réinitialiser le fichier des cartes tirées
> $fichier_tirage

#on réinitilialise le fichier des cartes du joueurs 1 et des cartes neutres
> $fichier_cartes_neutres
> $fichier_cartes_joueur1

#distribution de la premiere main
for numero_joueur in $(seq 1 $nombre_de_joueurs)
do
	#1ere carte
	premiereCarte=$(distribution)
	echo "$premiereCarte"

	#2eme carte
	deuxiemeCarte=$(distribution)
        echo "$deuxiemeCarte"

	if [[ $numero_joueur -eq 1 ]]
	then
		echo "$premiereCarte" >> $fichier_cartes_joueur1
		echo "$deuxiemeCarte" >> $fichier_cartes_joueur1
	fi

	#analyse de la main du joueur connu (joueur 1)
	analyse="$(analyseMain $premiereCarte $deuxiemeCarte |tail -1)"
	echo "analyse = $analyse"

done









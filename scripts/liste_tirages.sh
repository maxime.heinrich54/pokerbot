#!/bin/bash
#liste les tirages possibles pour 2 joueurs
fichier_liste_tirages="../tmp/liste_tirages"
#pour éviter la production de doublon, on affiche les cartes dans l'ordre croissant
for carte1 in $(seq 1 48)
do
	for carte2 in $(seq $((carte1+1)) 49)
	do
		for carte3 in $(seq $((carte2+1)) 50)
		do
			for carte4 in $(seq $((carte3+1)) 51)
			do
				for carte5 in $(seq $((carte4+1)) 52)
				do
					echo "ecriture de la ligne $carte1;$carte2;$carte3;$carte4;$carte5"
					echo "$carte1;$carte2;$carte3;$carte4;$carte5" >> $fichier_liste_tirages
				done
			done
		done
	done
done

